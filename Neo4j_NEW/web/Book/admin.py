from django.contrib import admin as dj_admin
from django_neomodel import admin as neo_admin

from .models import  *
class BookAdmin(dj_admin.ModelAdmin):
    list_display = ('title', 'published')
neo_admin.register(Book, BookAdmin)


class UserAdmin(dj_admin.ModelAdmin):
    list_display = ('fullname', 'email','username')
neo_admin.register(User, UserAdmin)




